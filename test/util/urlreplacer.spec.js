'use strict';

const expect = require("chai").expect;

// Unit under testing
const urlreplacer = require(__dirname + '/../../lib/util/urlreplacer');

describe("Url Replacer Util", function(){
  it('none', function(){
    let res = urlreplacer("test", ["fail"]);
    expect(res).to.eql('test');
  });

  it('one', function(){
    let res = urlreplacer("test{1}", ["fail"]);
    expect(res).to.eql('testfail');
  });

  it('invalid', function(){
    let res = urlreplacer("test{2}", ["fail"]);
    expect(res).to.eql('test{2}');
  });

  it('empty', function(){
    let res = urlreplacer("test{1}", []);
    expect(res).to.eql('test{1}');
  });

  it('many', function(){
    let res = urlreplacer("test{1}{2}{3}", ["i", "am", "sam"]);
    expect(res).to.eql('testiamsam');
  });

  it('numbers', function(){
    let res = urlreplacer("test{1}{2}{3}", [1, 2, 3]);
    expect(res).to.eql('test123');
  });

  it('urlencode', function(){
    let res = urlreplacer("test{1}", [","]);
    expect(res).to.eql('test%2C');
  });

  it('array value', function(){
    let res = urlreplacer("{1}", [ ["1234", "5678"] ]);
    expect(res).to.eql('(1234,5678)')
  });

  it('array value one', function(){
    let res = urlreplacer("{1}", [ ["1234"] ]);
    expect(res).to.eql("(1234)");
  });
});
