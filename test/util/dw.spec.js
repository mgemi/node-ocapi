'use strict';

const expect = require("chai").expect;

const defaults = require(__dirname + '/../../lib/defaults');

// Unit under testing
const dw = require(__dirname + '/../../lib/util/dw');

describe("Demandware Utils", function(){
  let dwHelper1, dwHelper2;

  beforeEach(function(){
    let dwConfigFixture1 = Object.assign({}, defaults, {
      siteName: false,
      host: "demo.group.realm.demandware.net",
      shopApiVersion: 'v14_8',
      dataApiVersion: 'v14_8',
      clientId: 'test-client-id-1',
      clientPassword: 'myPassword',
      bmUser: 'myapiuser',
      bmPassword: 'override-password'
    });

    let dwConfigFixture2 = Object.assign({}, defaults, {
      siteName: 'fixture-site',
      host: "sandbox01.group.realm.demandware.net",
      shopApiVersion: 'v17_2',
      dataApiVersion: 'v17_2',
      ignoreCertificateErrors: true,
      clientId: 'test-client-id-2'
    });

    dwHelper1 = dw.helpers(dwConfigFixture1, dwConfigFixture1.siteName, 'v14_8');
    dwHelper2 = dw.helpers(dwConfigFixture2, dwConfigFixture2.siteName, 'v17_2');
  });

  describe('test getBaseUrl', function(){
    it('fixture 1', function(){
      expect(dwHelper1.getBaseUrl('shop', {}))
        .to.eql('http://demo.group.realm.demandware.net/dw/shop/v14_8');
    });
    it('fixture 2', function(){
      expect(dwHelper2.getBaseUrl('shop', {ssl: false}))
        .to.eql('http://sandbox01.group.realm.demandware.net/s/fixture-site/dw/shop/v17_2');
    });
    it('fixture 2 data', function(){
      expect(dwHelper2.getBaseUrl('data', {ssl: false}))
        .to.eql('http://sandbox01.group.realm.demandware.net/s/-/dw/data/v17_2');
    });
    it('ssl', function(){
      expect(dwHelper1.getBaseUrl('data', {ssl: true}))
        .to.eql('https://demo.group.realm.demandware.net/dw/data/v14_8');
    })
  });

  describe('test formatPath', function(){
    it('one var', function(){
      expect(dwHelper1.formatPath('/products/{1}', {urlVars: ["mysku"]}))
        .to.eql('/products/mysku');
    });
    it('two var', function(){
      expect(dwHelper1.formatPath('/products/{1}/{2}', {urlVars: ["hello", "world"]}))
        .to.eql('/products/hello/world');
    });
    it('no var', function(){
      expect(dwHelper2.formatPath('/product'))
        .to.eql('/product');
    });
    it('empty', function(){
      expect(dwHelper2.formatPath('product', {urlVars: [], irrelevant: true}))
        .to.eql('/product');
    });
    it('url encode', function(){
      expect(dwHelper1.formatPath('/product/{1}', {urlVars: ["chris's, party"]}))
        .to.eql('/product/chris\'s%2C%20party');
    });
  });

  describe('test headers', function(){
    it('multiple', function(){
      expect(dwHelper1.headers({headers: {"x-organization": 1, "x-api-key": "12345"}})).to.eql({
        "Content-Type": "application/json",
        'x-dw-client-id': 'test-client-id-1',
        "x-organization": 1,
        "x-api-key": "12345"
      })
    });
    it('override', function(){
      expect(dwHelper2.headers({headers: {"Content-Type": "application/x-www-form-urlencoded"}})).to.eql({
        "Content-Type": "application/x-www-form-urlencoded",
        'x-dw-client-id': 'test-client-id-2'
      })
    });
    it('none', function(){
      expect(dwHelper1.headers({headers: {}})).to.eql({
        "Content-Type": "application/json",
        'x-dw-client-id': 'test-client-id-1'
      })
    });
    it('empty', function(){
      expect(dwHelper2.headers({})).to.eql({
        "Content-Type": "application/json",
        'x-dw-client-id': 'test-client-id-2',
      })
    });
  });

  describe('test buildUrl', function(){
    it('test 1', function(){
      expect(dwHelper1.buildUrl('shop', 'products/{1}', {
        headers: {'x-organization': 1},
        urlVars: ['12345'],
        queryParams: {hello: 'world'}
      })).to.eql('http://demo.group.realm.demandware.net/dw/shop/v14_8/products/12345');
    })

    it('test 2', function(){
      expect(dwHelper2.buildUrl('data', 'products/{1}', {
        ssl: true,
        urlVars: ['494']
      })).to.eql('https://sandbox01.group.realm.demandware.net/s/-/dw/data/v17_2/products/494');
    });

  });

  describe('test dataOauthCreds', function(){
    it('override credentials', function(){
      expect(dwHelper1.dataOauthCreds()).to.eql("Basic dGVzdC1jbGllbnQtaWQtMTpteVBhc3N3b3Jk");
    });

    it('default credentials', function(){
      expect(dwHelper2.dataOauthCreds()).to.eql("Basic dGVzdC1jbGllbnQtaWQtMjphYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWE=");
    });
  });

  describe('test shopOauthCreds', function(){
    it('override credentials', function(){
      expect(dwHelper1.shopOauthCreds()).to.eql("Basic bXlhcGl1c2VyOm92ZXJyaWRlLXBhc3N3b3JkOm15UGFzc3dvcmQ=");
    });

    it('default credentials', function(){
      expect(dwHelper2.shopOauthCreds()).to.eql("Basic d2Vic2VydmljZTpwYXNzd29yZCEyMzphYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWE=");
    });
  })

  describe('test dataOauthUrl', function(){
    it('default', function(){
      expect(dwHelper1.dataOauthUrl()).to.eql('https://account.demandware.com/dw/oauth2/access_token');
    });
  });

  describe('test shopOauthUrl', function(){
    it('default', function(){
      expect(dwHelper1.shopOauthUrl()).to.eql('https://demo.group.realm.demandware.net/dw/oauth2/access_token');
    });
  });

  describe('test enforceStrictHttps', function(){
    it('default', function(){
      expect(dwHelper1.enforceStrictHttps()).to.eql(true);
    });

    it('override', function(){
      expect(dwHelper2.enforceStrictHttps()).to.eql(false);
    })
  })
})
