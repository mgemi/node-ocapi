'use strict';

const expect = require("chai").expect;
const Auth = require(__dirname + '/../../lib/util/auth');

const dw = require(__dirname + '/../../lib/util/dw');
const defaults = require(__dirname + '/../../lib/defaults');

describe("Auth Utils", function(){
  let authorizerShop, authorizerData, httpAuthResponse;
  this.timeout(5000);

  beforeEach(function(){
    authorizerShop = new Auth(dw.helpers(defaults), 'shop');
    authorizerData = new Auth(dw.helpers(defaults), 'data');
  });

  it('token caching - no token', function(){
    expect(authorizerShop.__getCachedAccessToken()).to.be.equal(null);
    expect(authorizerData.__getCachedAccessToken()).to.be.equal(null);
  });

  it('token caching - basic caching', function(){
    authorizerShop.__cacheAccessToken("12345");
    expect(authorizerShop.__getCachedAccessToken()).to.be.equal("12345");

    authorizerData.__cacheAccessToken("123456");
    expect(authorizerData.__getCachedAccessToken()).to.be.equal("123456");
  });

  it('token caching - custom cache lifetime for shop', function(done){
    authorizerShop.__cacheAccessToken("12345", 10);
    setTimeout(function(){
      expect(authorizerShop.__getCachedAccessToken()).to.be.equal("12345");
      done();
    }, 1000);
  });

  it('token caching - custom cache lifetime for data', function(done){
    authorizerData.__cacheAccessToken("123456", 10);
    setTimeout(function(){
      expect(authorizerData.__getCachedAccessToken()).to.be.equal("123456");
      done();
    }, 1000);
  })

  it('token caching - expire - shop', function(done){
    authorizerShop.__cacheAccessToken("12345", 2);
    setTimeout(function(){
      expect(authorizerShop.__getCachedAccessToken()).to.be.equal(null);
      done();
    }, 3000);
  });

  it('token caching - expire - data', function(done){
    authorizerData.__cacheAccessToken("1234", 2);
    setTimeout(function(){
      expect(authorizerData.__getCachedAccessToken()).to.be.equal(null);
      done();
    }, 3000);
  });
});
