![Build Status](https://codeship.com/projects/f9734de0-dd12-0134-c760-46bf8a40c550/status?branch=master)
# node-ocapi

A simple node wrapper for Demandware's OCAPI apis.

## Currently supported versions:
* 14.8
* 15.1
* 17.2
* 17.3

## Usage

node-ocapi allows you to access ocapi resources through use of friendly names

Calling a Demandware method with either of these mechanics returns a Promise object.

### Initialization

Initialization is relatively straightforward, the NodeOcapi object is initialized like this:

```javascript
// Load OCAPI node module
const NodeOcapi = require('node-ocapi');

// Configure OCAPI to work with your instance of Demandware
let ocapi = NodeOcapi({
  siteName: "my-site",
  host: "sandbox.group.realm.demandware.net",
  shopApiVersion: 'v14_8',
  dataApiVersion: 'v17_2',
  clientId: 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
  clientPassword: 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
  bmUser: 'webservice',
  bmPassword: 'password!23',
  ignoreCertificateErrors: false
});
```

It takes the following parameters:

* `host : String` - The Base URL for the OCAPI endpoint of your Demandware instance.  Note that this is not the standard _instance-group-realm.demandware.net_ host but rather the custom _instance.group.realm.demandware.net_ url used by OCAPI.
* `siteName : String|false` - the name of the site used for OCAPI, IF you are using the standard naming convention to point to your site.  This allows you to generate a url in the format _http://instance.group.realm.demandware.net/s/my-site_.  If you have a CNAME to your site, i.e. _http://my-site.com/_, set this to false.
* `shopApiVersion : String` - the OCAPI SHOP API version configured within your Open Commerce API settings in business manager your site uses.
* `dataApiVersion : String` - the OCAPI DATA API version configured within your Open Commerce API settings in business manager your site uses.
* `clientId : String` - the key for the OCAPI client in Demandware, which your administrator setup in account.demandware.com, and is being used in the Business Manager OCAPI configurations
* `clientPassword : String` - The administrator password of the user who setup the clientId in account.demandware.com.
* `bmUser : String` - A business manager user account login for the business manager account that should have access to the shop API authentication.
* `bmPassword : String` - The password for the business manager user account that should have access to the shop API authentication.
* `ignoreCertificateErrors : Boolean` - A debug flag for ignoring certificate errors, for testing only.


### Using Resources

Resources are defined within lib/api/<version>/data and lib/api/<version>/shop.  These map to the API docs defined on documentation.demandware.com.  They provide human readable access for each of those resources available.  For example:

```javascript
// SHOP API: GET /products/:id
ocapi.shop.getProduct('mySku').then( ... );

// DATA API: GET /global_preferences/preference_groups/:group_id/:instance_type
ocapi.data.getGlobalPreference('1234567890', 'global').then( ... );
```

Each resources takes four parameters:

#### ocapi.<shop|data>.<RESOURCE>(urlParams, payload, queryParams, options)

* `urlParams : Array|String` - Parameters to replace in the URL, for instance /products/MYSKU
* `payload : Object` - A HTTP body payload to be sent with POST, PUT, PATCH requests
* `queryParams : Object` - Any additional query params (for pagination, etc) to be included as query parameters
* `options : Object` - Any additional options:
    * `options.headers : Object` - Specify any custom headers for the request (Defaults to request-level headers)
    * `options.ssl : Boolean` - Use SSL on this request (Defaults to request-level configuration)
    * `options.auth : Boolean` - Use authentication mechanism on this request (Defaults to request-level configuration)

### Creating new Resources

The library has deliberately been left empty in terms of included resources for users to fill out as needed.  To create a resource, navigate to the appropriate API version that matches the API you're using, for instance: lib/api/v17_2/shop/orders for the SHOP ORDERS APIs.

To create a new API, you will create a function within the module scope function:

Example:
```javascript
module.exports = function(app){
  app.get('getOrder', '/orders/{1}', {ssl: true, auth: true});
}
```

Functions available:
* *app.get* - Setup a GET route
* *app.post* - Setup a POST route
* *app.put* - Setup a PUT route
* *app.patch* - Setup a PATCH route
* *app.delete* - Setup a DELETE route

All these functions take the same parameters:

app.get(friendlyName, route, options)

* `friendlyName : String` - The "friendly name" used to call this route from the frontend, i.e.:  ocapi.data.friendlyName()
* `route : String` - The route of the Demandware backend route.  This can include dynamic parameters in the format {N} starting at one.  These will need to be passed into the friendly name as the urlParams parameter.
* `options : Object` - Additional, default options for the route (can be overridden in the resource call), including headers, auth, and ssl.

## TODO

* Most API resources empty: I haven't filled out most of the API resources, fill out as needed!
