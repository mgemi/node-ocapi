'use strict';
const OCAPI = require('./lib/ocapi');

/**
 * Demandware OCAPI API Bootstrap
 *
 * @param {String|Boolean} config.siteName If set to false, no site name included in addition to the config.host.  If set to a string, will include "/s/siteName" after the host. Defaults to false
 * @param {String}         config.host Demandware OCAPI host url, in the format sandbox.realm.account.demandware.net.
 * @param {String}         config.shopApiVersion OCAPI Shop API version, in the URL format. Defaults to 'v17_2'
 * @param {String}         config.shopApiVersion OCAPI Data API version, in the URL format. Defaults to 'v17_2'
 * @param {String}         config.clientId       The configured client_id in Demandware OCAPI settings
 * @param {String}         config.clientPassword The associated password with the supplied client_id (for your account.demandware.com account)
 * @param {String}         config.bmUser         Business manager username to login with for shop API auth
 * @param {String}         config.bmPassword     Business manager password for login for the shop API auth
 * @param {Boolean}        config.ignoreCertificateErrors Flag for ignoring SSL errors (for testing only!)
 *
 * @return {ocapi}         Returns an OCAPI object instance for calling the shop and data APIs.
 *
 * @author cbrousseau@mgemi.com
 */
module.exports = function(config){
  if (!config) {
    throw new Error("Missing configuration for OCAPI.");
  }
  return new OCAPI(config);
}
