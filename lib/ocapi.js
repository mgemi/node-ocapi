'use strict';

const defaultConfig = require('./defaults');
const Api = require('./util/api');
const dw = require('./util/dw');

/**
 * OCAPI main class
 * @param  {Object} clientConfig defined by the user
 * @see ../index.js for config options and default.js for defaults
 */
module.exports = function OCAPI(clientConfig){
  let shopApi, dataApi;

  __construct();

  return {
    shop: shopApi.routes,
    data: dataApi.routes
  }

  function __construct(){
    let config = Object.assign({}, defaultConfig, clientConfig);
    let dwHelper = dw.helpers(config);

    shopApi = buildShopApi(dwHelper);
    dataApi = buildDataApi(dwHelper);
  }

  function buildShopApi(dwHelper){
    return new Api(dwHelper, 'shop');
  }

  function buildDataApi(dwHelper){
    return new Api(dwHelper, 'data');
  }
}
