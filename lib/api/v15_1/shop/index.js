module.exports = {
  account: require('./account'),
  baskets: require('./baskets'),
  categories: require('./categories'),
  content: require('./content'),
  contentSearch: require('./contentSearch'),
  folders: require('./folders'),
  orders: require('./orders'),
  products: require('./products'),
  productSearch: require('./productSearch'),
  site: require('./site'),
  stores: require('./stores')
}
