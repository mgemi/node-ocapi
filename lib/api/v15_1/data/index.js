module.exports = {
  customers: require('./customers'),
  customerSearch: require('./customerSearch'),
  libraries: require('./libraries')
}
