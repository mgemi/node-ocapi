module.exports = function(app){
  app.get('getProduct', '/products/{1}', {auth: true, ssl: true});
  app.delete('deleteProduct', '/products/{1}', {auth: true, ssl: true});
  app.put('createProduct', '/products/{1}', {auth: true, ssl: true});
  app.patch('updateProduct', '/products/{1}', {auth: true, ssl: true});
  app.get('getProductVariationGroups', '/products/{1}/variation_groups', {auth: true, ssl: true});
  app.get('getProductVariations', '/products/{1}/variations', {auth: true, ssl: true});
  app.get('getProductVariationAttributes', '/products/{1}/variation_attributes', {auth: true, ssl: true});
}
