module.exports = {
  baskets: require('./baskets'),
  categories: require('./categories'),
  content: require('./content'),
  contentSearch: require('./contentSearch'),
  customers: require('./customers'),
  customObjects: require('./customObjects'),
  folders: require('./folders'),
  giftCertificate: require('./giftCertificate'),
  orders: require('./orders'),
  orderSearch: require('./orderSearch'),
  priceAdjustmentLimits: require('./priceAdjustmentLimits'),
  productLists: require('./productLists'),
  products: require('./products'),
  productSearch: require('./productSearch'),
  promotions: require('./promotions'),
  searchSuggestion: require('./searchSuggestion'),
  sessions: require('./sessions'),
  site: require('./site'),
  stores: require('./stores')
}
