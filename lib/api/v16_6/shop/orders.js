module.exports = function(app){
  app.get('getOrder', '/orders/{1}', {ssl: true, auth: true});
  app.post('createOrder', '/orders', {ssl: true, auth: true});
}
