module.exports = function(app){
  app.post('createBasket', '/baskets', {ssl: true, auth: true});
  app.patch('updateBasket', '/baskets/{1}', {ssl: true, auth: true});
  app.get('getBasket', '/baskets/{1}', {ssl: true, auth: true});
  app.put('addCustomer', '/baskets/{1}/customer', {ssl: true, auth: true});
  app.put('addShippingAddress', '/baskets/{1}/shipments/me/shipping_address', {ssl: true, auth: true});
  app.put('addBillingAddress', '/baskets/{1}/billing_address', {ssl: true, auth: true});
  app.get('getShipMethods', '/baskets/{1}/shipments/me/shipping_methods', {ssl: true, auth: true});
  app.put('addShipMethod', '/baskets/{1}/shipments/me/shipping_method', {ssl: true, auth: true});
  app.post('addItem', '/baskets/{1}/items', {ssl: true, auth: true});
  app.post('addPayment', '/baskets/{1}/payment_instruments', {ssl: true, auth: true});
  app.post('addCoupon', '/baskets/{1}/coupons', {ssl: true, auth: true});
}
