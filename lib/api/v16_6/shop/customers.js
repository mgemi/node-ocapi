module.exports = function(app){
  app.post('register', '/customers', {ssl: true, auth: true});
  app.post('login', '/customers/auth', {ssl: true, auth: true, body: {type: 'guest'}});
}
