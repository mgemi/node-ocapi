'use strict';

/**
 * Demandware OCAPI API default parameters
 * @type {Object}
 */
module.exports = {

  // OCAPI configuration defaults
  siteName: false,
  host: "sandbox.group.realm.demandware.net",
  shopApiVersion: 'v17_2',
  dataApiVersion: 'v17_2',

  // Debug flag for handling cert errors in non-production environments
  ignoreCertificateErrors: false,

  // OCAPI authorization defaults
  clientId: 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
  clientPassword: 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
  bmUser: 'webservice',
  bmPassword: 'password!23',

  // Digital server defaults
  digitalAuthServer: 'account.demandware.com',
  digitalAuthServerAuthResource: '/dw/oauth2/access_token',
  digitalAppServerAuthResource: '/dw/oauth2/access_token'
}
