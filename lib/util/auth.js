'use strict';

const http = require('request');
const Promise = require('bluebird');
const httpAsync = Promise.promisify(http);

const AuthTypeFactory = require('./authtype');

/**
 * Authorizer - used for resource authentication
 * @param  {Object} dwHelper Demandware helpers
 * @param  {String} apiScope API type (shop or data)
 */
module.exports = function Authorizer(dwHelper, apiScope){
  const EXPIRATION_BUFFER = 60;
  const MAX_TOKEN_LIFETIME = 3600;

  let cachedAccessToken,
      cachedAccessTokenExpiration,
      authType;

  __construct();

  return {
    auth: auth,
    __getCachedAccessToken: getCachedAccessToken,
    __cacheAccessToken: cacheAccessToken,
    __call: call
  }

  function __construct(){
    authType = AuthTypeFactory(dwHelper, apiScope);
  }

  /**
   * Entrypoint
   * @return {Promise}
   */
  function auth(){
    let token = getCachedAccessToken();

    if (token) {
      return Promise.resolve(token);
    } else {
      return call().then(authenticationData => {
        // @todo other types of tokens?
        if (authenticationData.token_type == 'Bearer'){
          token = `Bearer ${authenticationData.access_token}`;
        }

        cacheAccessToken(token, authenticationData.expires_in);
        return token;
      });
    }
  }

  /**
   * Caching access token in order to reduce unnecessary calls to the authentication server
   * @see cacheAccessToken()
   * @return {String}
   */
  function getCachedAccessToken(){
    if (!!cachedAccessToken && cachedAccessTokenExpiration > now()){
      return cachedAccessToken;
    }
    return null;
  }

  /**
   * Sets expiration date for the token for caching access token in memory
   * @param {Integer} expiresIn Number of seconds from now for token to expire
   */
  function cacheAccessToken(token, expiresIn){
    cachedAccessToken = token;
    if (!expiresIn) {
      expiresIn = MAX_TOKEN_LIFETIME - EXPIRATION_BUFFER;
    }
    cachedAccessTokenExpiration = now() + expiresIn;
  }

  /**
   * UTC Timestamp in seconds
   * @return {Integer}
   */
  function now(){
    return Math.floor((new Date()).getTime() / 1000);
  }

  /**
   * Authorize against demandware for DATA services
   * @return {Promise}
   */
  function call(){
    return httpAsync({
      method: authType.getMethod(),
      uri: authType.getUrl(),
      headers: {
        'Content-Type': authType.getContentType(),
        'Authorization': authType.getAuthorization(),
      },
      form: {
        'grant_type': authType.getGrantType()
      },
      strictSSL: dwHelper.enforceStrictHttps(),
      qs: authType.getQueryParameters()
    }).then(res => {
      if(res.statusCode >= 500) {
        console.error('500 Error returned from OCAPI', res.body);
        throw new Error('Demandware server error');
      } else if(res.statusCode !== 200) {
        console.warn('Error with Authorization in OCAPI', res.body);
        throw new Error('Authorization error over OCAPI -- please validate credentials and permissions within Demandware.')
      }
      return JSON.parse(res.body);
    });
  }

}
