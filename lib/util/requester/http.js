'use strict';

const http = require('request');
const Promise = require('bluebird');
const httpAsync = Promise.promisify(http);

/**
 * Call http request
 * Implementation using npm request module
 * @param  {Object} cfg
 * @see Requester.call()
 * @return {Promise}
 */
module.exports = function call(cfg){

  // Set json to true for GET and DELETE methods, which do not have a payload
  // so we can get the response back as json.
  let body = cfg.body;
  if (cfg.method == "GET" || cfg.method == "DELETE" || !cfg.body) {
    body = true;
  }

  return httpAsync({
    method: cfg.method,
    uri: cfg.url,
    headers: cfg.headers,
    json: body,
    qs: cfg.queryParams,
    strictSSL: cfg.strictSSL
  }).then(res => {
    return {
      status: res.statusCode,
      body: res.body
    }
  });
}
