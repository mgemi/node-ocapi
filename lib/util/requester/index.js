'use strict';

let requester;

module.exports = {
  /**
   * Wrapper method for calling the request (over the appropriate delivery handler)
   * @param  {Object} cfg             Request configuration
   * @param  {String} cfg.method      HTTP Method (POST, GET, etc)
   * @param  {String} cfg.url         Url to request against
   * @param  {Object} cfg.queryParams Query Parameters to add to the request
   * @param  {Object} cfg.headers     Headers object
   * @param  {Object} cfg.body        Body object
   * @param  {Boolean} cfg.strictSSL  Use strict SSL flag
   * @return {Promise}
   */
  call: function(cfg) {
    let requesterMethod = getRequester();
    return requesterMethod(cfg);
  }
}

function getRequester(){
  if (!requester) {
    // TODO determine handler type
    return getHttpHandler();
  }
  return requester;
}

function getHttpHandler(){
  return require('./http');
}

function getXhrHandler(){
  return require('./xhr');
}
