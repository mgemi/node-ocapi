'use strict';

const _ = require('lodash');
const List = require('ensurelist');
const Resource = require('./resource');
const Authorizer = require('./auth');


/**
 * OCAPI API manager
 *
 * This is where mose of the magic happens.  Instantiating this object will load
 * all routes within the API path (see routes/*).
 *
 * See the index.js functions to see all the module loading.  Each module should
 * load a function which allows routes to be set by using the resource mapper (see loadRoutes)
 *
 * The idea is that routes will be exposed in user friendly methods to the user.
 * So you could call, if you set it up in the child resources:
 *   let myApi = new Api();
 *   myApi.getProducts();
 * This lib also manages conflicts in friendly names.
 *
 * @param  {Object} dwHelper    Demandware helper object
 * @param  {String} apiScope Path to the API route, shop or data
 */
module.exports = function Api(dwHelper, apiScope){
  let routes = {},
      authorizer,
      modules;

  __construct();

  return {
    routes: routes,
    __methodWrapper: methodWrapper,
    ___route: _route
  }

  /**
   * Load all routes from routes/{apiScope}/
   * @param  {Object|Array} modules From routes/{apiScope}/index.js
   * @return {Array}
   */
  function __construct(){
    modules = require(`../api/${dwHelper.getApiVersion(apiScope)}/${apiScope}`);

    authorizer = new Authorizer(dwHelper, apiScope);
    return _.map(modules, function(resource){
      return resource({
        get: methodWrapper("GET"),
        post: methodWrapper("POST"),
        delete: methodWrapper("DELETE"),
        put: methodWrapper("PUT"),
        patch: methodWrapper("PATCH")
      });
    });
  }

  /**
   * Wrapper to aggregate each method (get/post/etc) to the same handler
   * @param {String} method Method type (GET, POST, ETC)
   * @return Function
   */
  function methodWrapper(method){
    return function(friendlyName, path, config){
      return _route(method, friendlyName, path, config);
    }
  }

  /**
   * Route builder based on feedback from individual routers
   * @param  {String} method       HTTP Method (GET, POST, etc)
   * @param  {String} friendlyName A user-friendly name for this method, to be called by the user.
   * @param  {String} path         Resource path in OCAPI
   * @param  {Object} routeOptions Request Options on the route (params, headers, etc).
   * @return {Function}            Route handling and calling
   */
  function _route(method, friendlyName, path, routeOptions){
    if(!routeOptions) routeOptions = {};
    if (routes[friendlyName]) {
      throw new Error(`Namespace conflict: Trying to create API ${friendlyName} for ${path} failed because it already exists for ${routes.path}.`)
    }

    /**
     * This is what a user calls from their application
     */
    let r = function(urlVars, payload, queryParams, reqOptions){
      const options = Object.assign({}, routeOptions, reqOptions);
      options.urlVars = List(urlVars);
      options.body = payload;

      let resource = new Resource(dwHelper, authorizer, apiScope);
      return resource.call(method, path, queryParams, options);
    }

    routes[friendlyName] = r;
    return r;
  }
}
