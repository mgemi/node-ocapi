'use strict';

/**
 * Url Replacer
 * Replaces substrings {1},{2},...{N} with passed array of variables.
 * For use in urls
 *
 * @param  {String} source       Base URL path parameters to {1}, {2}, etc values
 * @param  {Array}  replacements Replacements corresponding to the placeholders in url
 * @return {String}              Constructed URL with filled in placeholders
 *
 * Call with require('./urlreplacer')(args, ...);
 */
module.exports = function(source, replacements){
  let result = source;

  replacements.forEach((i, k) => {
    let val;
    if (Array.isArray(i)) {
      val = "(" + i.map(j => encodeURIComponent(j)).join(',') + ")";
    } else {
      val = encodeURIComponent(i);
    }
    result = result.replace("{" + (k+1) + "}", val);
  });

  return result;
}
