'use strict';

module.exports = function DataAuthType(dwHelper){
  return {
    getMethod: function(){
      return 'POST';
    },

    getUrl: function(){
      return dwHelper.dataOauthUrl();
    },

    getContentType: function(){
      return 'application/x-www-form-urlencoded';
    },

    getAuthorization: function(){
      return dwHelper.dataOauthCreds();
    },

    getGrantType: function(){
      return 'client_credentials';
    },

    getQueryParameters: function(){
      return {}
    }
  }
}
