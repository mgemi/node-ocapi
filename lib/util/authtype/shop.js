'use strict';

module.exports = function ShopAuthType(dwHelper){
  return {
    getMethod: function(){
      return 'POST';
    },

    getUrl: function(){
      return dwHelper.shopOauthUrl();
    },

    getContentType: function(){
      return 'application/x-www-form-urlencoded';
    },

    getAuthorization: function(){
      return dwHelper.shopOauthCreds();
    },

    getGrantType: function(){
      return 'urn:demandware:params:oauth:grant-type:client-id:dwsid:dwsecuretoken';
    },

    getQueryParameters: function(){
      return {
        'client_id': dwHelper.getClientId()
      }
    }
  }
}
