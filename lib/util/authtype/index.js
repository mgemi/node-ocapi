'use strict';

/**
 * AuthTypeFactory()
 * Factory creation method for auth helpers that distinguish the functionality
 * for authorization requests for shop and data
 *
 * @param  {Object} dwHelper Demandware OCAPI Helper Object
 * @param  {String} apiScope [description]
 * @return {Object}          auth type helper
 */
module.exports = function AuthTypeFactory(dwHelper, apiScope){
  let AuthCls;

  if (apiScope == 'shop') {
    AuthCls = require('./shop');
  } else if (apiScope == 'data') {
    AuthCls = require('./data');
  } else {
    throw new Error(`Invalid apiScope type '${apiScope}' in AuthTypeFactory`);
  }

  return new AuthCls(dwHelper);
}
