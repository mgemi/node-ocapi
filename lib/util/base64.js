'use strict';

/**
 * Convert string to base 64
 * @param  {String} str     any old string
 * @return {String}         same string encoded in base 64
 */
module.exports = function base64(str){
  let buffer64 = new Buffer(str);
  return buffer64.toString('base64');
}
