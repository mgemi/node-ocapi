'use strict';

const urlreplacer = require('./urlreplacer');
const base64 = require('./base64');

/**
 * Demandware OCAPI helpers object
 * Call with require('./dw').helpers;
 *
 * @param {Object} config configuration options, see default.js
 * @param {String} siteId
 * @param {String} apiVersion
 * @type {Object}
 */
module.exports = {
  helpers: function(config){
    return {
      /**
       * get session client id
       * @return {String}
       */
      getClientId: function(){
        return config.clientId;
      },

      /**
       * Build a url to demandware OCAPI based on an apiScope, path, and options
       * @param  {String} apiScope   API type (shop or data)
       * @param  {String} path       API resource path (/products/:id)
       * @param  {Object} options    API Options (ssl, params, headers, etc).  See resource.js
       * @return {String}            Fully formatted URL
       */
      buildUrl: function(apiScope, path, options){
        return this.getBaseUrl(apiScope, options) + this.formatPath(path, options);
      },

      /**
       * Get Demandware OCAPI base url based on configuration parameters.  See index.js
       * @param  {String} apiScope  API type (shop or data)
       * @param  {Object} options   Request Options (ssl, params, headers, etc).  See resource.js
       * @return {String}           Base URL for OCAPI
       */
      getBaseUrl: function(apiScope, options){
        return (options.ssl ? 'https://' : 'http://')
          + config.host
          + this.getBaseUrlSiteSubstring(apiScope)
          + `/dw/${apiScope}/${this.getApiVersion(apiScope)}`;
      },

      /**
       * Get URL substring of site for OCAPI url, this will be /s/SITENAME or ''
       * @param  {String} apiScope API type (shop or data)
       * @return {String}          Base URL substring
       */
      getBaseUrlSiteSubstring: function(apiScope){
        // If sitename was excluded in config, we are using a CNAME and this can be excluded
        if (config.siteName === false) {
          return '';
        }

        // Data has an overrided exclusion for sitename, manually force to '-' as it is global scope in DW
        if (apiScope === 'data') {
          return '/s/-';
        }

        // Shop uses standard sitename
        return `/s/${config.siteName}`;
      },

      /**
       * Get API Version based on apiScope (shop or data)
       * @param  {String} apiScope  API type (shop or data)
       * @return {String}           API version number
       */
      getApiVersion: function(apiScope){
        if (apiScope === 'data') {
          return config.dataApiVersion;
        } else if (apiScope === 'shop') {
          return config.shopApiVersion;
        } else {
          throw new Error(`Invalid apiScope ${apiScope} in dwHelper.getApiVersion()`);
        }
      },

      /**
       * Format resource path for API call
       * @param  {String} path    API resource path (/products/:id)
       * @param  {Object} options Request Options (params, headers, etc).  See resource.js
       * @return {String}         Resource URL for OCAPI
       */
      formatPath: function(path, options){
        if (options && options.urlVars) {
          path = urlreplacer(path, options.urlVars);
        }

        if (path.length > 0 && path[0] != "/") {
          path = "/" + path;
        }

        return path;
      },

      /**
       * Configure headers for OCAPI call, including client-set options
       * @param  {Object} options Request Options (params, headers, etc).  See resource.js
       * @return {Object}         Headers object
       */
      headers: function(options){
        let headers = {
          'Content-Type': 'application/json',
          'x-dw-client-id': config.clientId
        }

        if (options.headers) {
          headers = Object.assign(headers, options.headers);
        }

        return headers;
      },

      /**
       * Return authorization string with DATA API credentials
       * @return {String}
       */
      dataOauthCreds: function(){
        return 'Basic ' + base64([config.clientId, config.clientPassword].join(':'));
      },

      /**
       * Return authorization string with SHOP API credentials
       * @return {String}
       */
      shopOauthCreds: function(){
        return 'Basic ' + base64([config.bmUser, config.bmPassword, config.clientPassword].join(':'));
      },

      /**
       * Authorization endpoint for data API (on centralized demandware server)
       * @return {String}
       */
      dataOauthUrl: function(){
        return 'https://' + config.digitalAuthServer + config.digitalAuthServerAuthResource;
      },

      /**
       * Authorization endpoint for shop API (on client instance)
       * @return {String}
       */
      shopOauthUrl: function(){
        return 'https://' + config.host + config.digitalAppServerAuthResource;
      },

      /**
       * A lot of sandboxes do not use valid certificates.  Here's a flag to reject enforcing strict HTTPS
       * @return {Boolean}
       */
      enforceStrictHttps: function(){
        return !config.ignoreCertificateErrors;
      }
    }
  }
}
