'use strict';

const requester = require('./requester');

/**
 * Promisified wrapper for Demandware OCAPI resource.
 *
 * Configure the resource by calling new Resource(dw, apiScope)
 * Then call the resource with method, path, options
 *
 * @param {Object} dwHelper  Demandware helper object
 * @param {Function} Authorizer Authorization class
 * @param {String} apiScope   API type (shop or data)
 */
module.exports = function Resource(dwHelper, authorizer, apiScope){

  return {
    call: call,
    __callMethod: callMethod
  }

  /**
   * Call a resource with a specified method
   * @param  {String} method      HTTP Method (GET, POST, PUT, PATCH, DELETE)
   * @param  {String} path        API resource path (/products/:id)
   * @param  {Object} queryParams URL query params in a dictionary-type object
   * @param  {Object} options     Request Options
   *
   * @param {Object}  options.headers     HTTP Headers to include in request
   * @param {Boolean} options.ssl         whether to send this request in SSL. defaults to false
   * @param {Boolean} options.auth        whether to authorize with an auth token
   *
   * @return {Promise}
   */
  function call(method, path, queryParams, options){
    if (options.auth) {
      return authorizer.auth(options).then(token => {
        if(!options.headers) options.headers = {};
        options.headers['Authorization'] = token;

        return callMethod(method, path, queryParams, options);
      });
    } else {
      return callMethod(method, path, queryParams, options);
    }
  }

  function callMethod(method, path, queryParams, options){
    let url = dwHelper.buildUrl(apiScope, path, options);

    console.log("Creating a request to Demandware URL: ", method, url);

    return requester.call({
      method: method,
      url: url,
      headers: dwHelper.headers(options),
      body: options.body,
      queryParams: queryParams,
      strictSSL: dwHelper.enforceStrictHttps()
    }).then(res => {
      if (res.status < 400) {
        return res.body;
      }

      if (res.body.fault) {
        console.warn(res.body.fault.message);
        throw new Error(res.body.fault.type);
      } else {
        console.error("Unknown Error with OCAPI", res.body);
        throw new Error("GenericError");
      }
    })
  }
}
